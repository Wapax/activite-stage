<html>

<head>
    <link rel="stylesheet" href="css/style.css">
    <title>Page de présentation</title>
</head>

<body>
    <p class="red">Dans cette partie, créez un texte de présentation sur vous (qui vous êtes, où vous habitez,
        ce
        que vous aimez, n'aimez pas,
    <div class=""></div>...) et ajouter une image que vous aimez. Stockez l'image dans
    <code>public/</code> et importez la.
    </p>
    <div class="container">
        <header>
            <?php
            include("modules/navbar.php");
            ?>
        </header>
        <div class="content">

        </div>
        <footer>
            <?php
            include("modules/footer.php");
            ?>
        </footer>
    </div>
</body>

</html>