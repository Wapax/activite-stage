<html>

<head>
    <title>Calcul</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/form.css">
</head>

<body>
    <!-- <span class="red">
        Utilisez dette page pour comprendre les formulaires. En suite, allez sur la page contact
    </span> -->
    <div class="container">
        <header>
            <?php include("modules/navbar.php") ?>
        </header>


        <div class="content">
            <h1>Calculatrice</h1>
            <form action="calcul.php" method="post">
                <input type="number" name="a" placeholder="Nombre a" required>
                <input type="number" name="b" placeholder="Nombre b" required>
                <select name="operation">
                    <option value="add">+</option>
                    <option value="div">÷</option>
                    <option value="mul">×</option>
                    <option value="sou">-</option>
                </select>
                <input type="submit" value="Calculer">
            </form>
            <?php

            if (!empty($_POST) && (!empty($_POST["a"]) || $_POST["a"] == 0) && (!empty($_POST["b"]) || $_POST["b"] == 0)) {

                switch ($_POST["operation"]) {
                    case "add":
                        echo '<span>Le résulatat du clacul ' . $_POST["a"] . ' + ' . $_POST["b"] . ' est ' . $_POST["a"] + $_POST["b"] . '</span>';
                        break;
                    case "mul":
                        echo '<span>Le résulatat du clacul ' . $_POST["a"] . ' x ' . $_POST["b"] . ' est ' . $_POST["a"] * $_POST["b"] . '</span>';
                        break;
                    case "div":
                        if ($_POST["b"] == 0) {
                            echo '<span>Division par zéro impossible</span>';
                        } else {
                            echo '<span>Le résulatat du clacul ' . $_POST["a"] . ' ÷ ' . $_POST["b"] . ' est ' . $_POST["a"] / $_POST["b"] . '</span>';
                        }
                        break;
                    case "sou":
                        echo '<span>Le résulatat du clacul ' . $_POST["a"] . ' - ' . $_POST["b"] . ' est ' . $_POST["a"] - $_POST["b"] . '</span>';
                        break;
                }
            } else if (!empty($_POST)) {
                echo '<span>Veuillez saisir des nombres</span>';
            }
            ?>
        </div>


        <footer>
            <?php include("modules/footer.php") ?>
        </footer>
    </div>
</body>

</html>