<html>

<head>
    <title>Contact</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/form.css">

<body>
    <span class="red">Ajoutez les champs de saisie pour l'adresse mail et le numéro de téléphone. Ajoutez un bouton pour
        vider le formulaire des données saisies. Une fois fini, cliquez sur le bouton "continuer"</span>
    <div class="container">
        <header>
            <?php
            include("modules/navbar.php");
            ?>
        </header>
        <div class="content">
            <form action="result.php" method="post">
                <label for="firstname">Prénom</label>
                <input type="text" name="prenom" id="firstname" placeholder="Prénom...">
                <label for="name">Nom</label>
                <input type="text" name="nom" id="name" placeholder="Nom...">
                <label for="message">Votre message</label>
                <textarea name="message" id="message" cols="30" rows="5"></textarea>
                <input type="hidden" name="source" value="contact">
                <input type="submit" value="Envoyer">
            </form>
            <div id="resume"></div>
            <script>
                const resumeButton = document.createElement("a");
                resumeButton.textContent = "Continuer";
                resumeButton.
                const dom = document.getElementById("resume");
                dom.appendChild(resumeButton);
            </script>
        </div>
        <footer>
            <?php
            include("modules/footer.php");
            ?>
        </footer>
    </div>
</body>

</html>