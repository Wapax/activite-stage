<html>

<body>

    <head>
        <link rel="stylesheet" href="css/result.css">
        <title>Résultat</title>
    </head>

    <h1>Cette page affiche le contenu de la requête qui a été renvoyée au serveur</h1>

    <?php
    if (!empty($_POST)) {
        echo '<table>';
        extract($_POST);
        foreach ($_POST as $key => $value) {
            echo "<tr><td>" . $key . "</td><td>" . $value . "</td></tr>";
        }
        echo '</table>';
        if (!empty($source))
            echo '<a class="link-button" href="' . $source . '.php"/><< Retrourner sur la page ' . $source;
    } else {
        echo "<h2>Le formulaire est vide</h2>";
    }
    ?>

</body>

</html>