<html>

<head>
    <title>Page d'accueil</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <header>
            <?php
            include("modules/navbar.php");
            ?>
        </header>
        <div class="content">
            <h1>Bienvenue sur le site pour débuter le développement web !</h1>
            <img src="public/structure_html.jpg" id="html-structure">
            <p>Ce site est composé de trois parties :</p>
            <h2>L'en-tête</h2>
            <p>C'est la barre grise qui se trouve tout en haut de la page. Elle contient les liens vers les autres pages
                permettant de naviguer sur le site, d'où son nom de <i>navbar.</i></p>
            <h2>Le corps</h2>
            <p>Le corps est la partie centrale de la page à l'intérieur de laquelle on ajoute le contenu du site. Le
                texte
                que vous lisez actuellement se trouve dans le corps.</p>
            <h2>Le pieds de page</h2>
            <p>C'est la partie tout en bas de la page. Elle contient notamment des informations sur le site, des
                raccourcis
                vers les différentes parties du site, les mentions légales, etc.</p>
            <h2>Consignes</h2>
            <p>Dans ces exercices, vous allez devoir modifier le code afin d'obternir le résultat attendu. Passez sur
                chaque
                page pour voir les défis qui vous attendent ! Les consignes sont écrites <b><span class="red"> en
                        rouge</span></b>
                en haut de l'en-tête. Vous devez ajouter votre travail dans la balise nommée <code>content</code>.</p>

        </div>
        <footer>
            <?php
            include("modules/footer.php");
            ?>
        </footer>
    </div>
</body>

</html>